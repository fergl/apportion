
function listToString(list) {
	var result = "";
	if(list != undefined && list != null && list.length > 0){
		for(var i = 0; i < list.length; i++){
			result = result + dictionary[list[i]] + ", ";
		}
		if(result.length > 2){
			result = result.substr(0, result.length - 2);
		}
	}
	return result;
}

function getMovie(qid){
	var data = movie_list[qid];
	$('body')[0].innerHTML = movie_body;

	document.getElementById('title').innerHTML = data.label;
	document.getElementById('year').innerHTML = data.date;
	document.getElementById('duration').innerHTML = data.duration;
	document.getElementById('gender').innerHTML = listToString(data.gender_ids);
	document.getElementById('director').innerHTML = listToString(data.director_ids);
	document.getElementById('screenwritter').innerHTML = listToString(data.screenwriter_ids);
	document.getElementById('cast').innerHTML = listToString(data.cast_ids);
	document.getElementById('countries').innerHTML = listToString(data.country_ids);
	document.getElementById('companies').innerHTML = listToString(data.companies_ids);
	document.getElementById('poster').src = getLangedValue(data.posters, "value");
	document.getElementById('plot').innerText = data.plot;

	var feature = "";
	if(catalog[qid] != undefined && catalog[qid] != null) {
		for(var i = 0; i < catalog[qid].feature.length; i++){
			if(catalog[qid].feature[i].status === 1){
				var au = "";
				for(var j = 0; j < catalog[qid].feature[i].audios.length ; j++){
					au += catalog[qid].feature[i].audios[j];
					if (au == data.original_language) {
						au += " (Original)";
					}
					au += ", ";
				}
				au = au.substr(0, au.length - 2);
				feature+= "<li><table style='width: 100%;'><tr><td><a href='" + catalog[qid].feature[i].url + "' target='blank'><div><div class='label'>" + dictionary[host_provider[getHost(catalog[qid].feature[i].url)]] + "</div><div class='info'><b>Audio: </b>" + au + "</div><div class='info'><b>Subtitles: </b>" + printSubtitles(catalog[qid].feature[i].subtitles) + "</div><div class='info'><b>" + (catalog[qid].feature[i].hq === 1 ? "HD - " : "") + "</b>" + "</div>" +

"</div></a></td><td style='width: 1%;'>" +

"<div class='grid-options'><a href='#' onClick=\"alert('TO BE DEVELOPED: Allows the user to edit source data -except for url-')\"><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Small_Pencil_Icon.svg/15px-Small_Pencil_Icon.svg.png?20220605164805'></a><a href='#' onClick=\"alert('TO BE DEVELOPED: Allows the user to remove a source -it will remain stored but unactive with the action date in order to override outdated new imports with the same source')\"><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Faenza-user-trash-symbolic.svg/16px-Faenza-user-trash-symbolic.svg.png?20180510195546'></a></div>" +

"</td></tr></table></li>";
			}
		}
	}
	document.getElementById('feature').innerHTML = feature;

	//EXTRAS
	var extras = "";
	if(catalog[qid] != undefined && catalog[qid] != null && catalog[qid].extras != undefined) {
		for(var i = 0; i < catalog[qid].extras.length; i++){
			for(var j = 0; j < catalog[qid].extras[i].audios.length ; j++){
				if(catalog[qid].extras[i].status === 1){
				var au = catalog[qid].extras[i].audios[j];
					extras+= "<li><a href='" + catalog[qid].extras[i].url + "' target='blank'><div><div class='label'>"  + catalog[qid].extras[i].label + " - " + catalog[qid].extras[i].provider + "</div><div class='info'><b>Audio: </b>" + au + "</div><div class='info'><b>Subtitles: </b>" + printSubtitles(catalog[qid].extras[i].subtitles) + "</div><div class='info'><b>Quality: </b>" + catalog[qid].extras[i].quality + "</div></div></a></li>";
				}
			}
		}
	}
	document.getElementById('featurette').innerHTML = extras;

	//PREVIEWS
	var previews = "";
	if(catalog[qid] != undefined && catalog[qid] != null && catalog[qid].previews != undefined) {
		for(var i = 0; i < catalog[qid].previews.length; i++){
			for(var j = 0; j < catalog[qid].previews[i].audios.length ; j++){
				if(catalog[qid].previews[i].status === 1){
				var au = catalog[qid].previews[i].audios[j];
					previews += "<li><a href='" + catalog[qid].previews[i].url + "' target='blank'><div><div class='label'>"  + catalog[qid].previews[i].label + " - [" + catalog[qid].previews[i].type + "] - " + catalog[qid].previews[i].provider + "</div><div class='info'><b>Audio: </b>" + au + "</div><div class='info'><b>Subtitles: </b>" + printSubtitles(catalog[qid].previews[i].subtitles) + "</div><div class='info'><b>Quality: </b>" + catalog[qid].previews[i].quality + "</div></div></a></li>";
				}
			}
		}
	}
	document.getElementById('previews').innerHTML = previews;

	//RELATED
	var html = "";
	var movie_previous = data.follows;
	var movie_next = data.followedBy;
	if(movie_previous != null && movie_previous != "" && movie_list[movie_previous].film == true){
		html += "<h2>Follows</h2>";
		var poster = getLangedValue(movie_list[movie_previous].posters, "value");
		if(catalog[movie_previous] != undefined){
			html += "<div class='menu-poster'><a href='#' onClick=\"getMovie('" + movie_previous + "')\"><img src='" + poster + "' /></a></div>";
		} else {
			html += "<div class='menu-poster'><img src='" + poster + "' style='opacity:0.25;' /></div>";
		}
	}

	if(movie_next != null && movie_next != "" && movie_list[movie_next].film == true){
		html += "<h2>Followed by</h2>";
		var poster = getLangedValue(movie_list[movie_next].posters, "value");
		if(catalog[movie_next] != undefined){
			html += "<div class='menu-poster'><a href='#' onClick=\"getMovie('" + movie_next + "')\"><img src='" + poster + "' /></a></div>";
		} else {
			html += "<div class='menu-poster'><img src='" + poster + "' style='opacity:0.25;' /></div>";
		}
	}

	var saga = data.collections_ids;
	if(saga != null){
		for(var i = 0; i < saga.length; i++){
			if(collection_list[saga[i]] == undefined){
				continue;
			}
			var innerHtml = "";
			var hasAMovie = false;
			if (saga != "follow") {
				innerHtml += "<h2>" + collection_list[saga[i]].sagaTitle + "</h2>";
			} else {
				innerHtml += "<h2>List of films</h2>";
			}
			for (var j = 0; j < collection_list[saga[i]].movieList.length; j++){
				var movieId = collection_list[saga[i]].movieList[j];
				if(movie_list[movieId].film == false){
					continue;
				} else {
					hasAMovie = true;
				}
				var poster = getLangedValue(movie_list[movieId].posters, "value");
				if(catalog[movieId] != undefined){
					innerHtml += "<div class='menu-poster'><a href='#' onClick=\"getMovie('" + movieId + "')\"><img src='" + poster + "' /></a></div>";
				} else {
					innerHtml += "<div class='menu-poster'><img src='" + poster + "' style='opacity:0.25;' /></div>";
				}
			}
			if(hasAMovie){
				html += innerHtml;
			}
	}
}

document.getElementById('collections').innerHTML = html;

	//links
	html = "";
	var valor = getLangedValue(data.wikis);
	var valorq = getLangedValue(data.quotes);
	if(valor != undefined && valor.length > 0){
		html += "<a href='" + valor + "' target='blank'>Wikipedia</a><br />";
	}
	if(valorq != undefined && valorq.length > 0){
		html += "<a href='" + valorq + "' target='blank'>Wikiquotes</a><br />";
	}
	if(data.commons != undefined && data.commons.length > 0){
		html += "<a href='" + data.commons[0] + "' target='blank'>Wikimedia Commons</a><br />";
	}
	html += "<a href='https://www.wikidata.org/wiki/" + qid + "' target='blank'>Wikidata</a><br />";
	document.getElementById('links').innerHTML = html;
}

function printSubtitles(list) {
	var result = "";
	if(list != undefined && list != null && list.length > 0){
		for(var i = 0; i < list.length; i++){
			result = result + list[i] + ", ";
		}
		if(result.length > 2){
			result = result.substr(0, result.length - 2);
		}
	}

	if(result.trim() == ""){
		result = " - ";
	}
	return result;
}


function section(name) {
	$('.section')[0].classList.remove('shown');
	$('.section')[1].classList.remove('shown');
	$('.section')[2].classList.remove('shown');
	$('.section')[3].classList.remove('shown');
	$('.section')[4].classList.remove('shown');
	$('.section')[5].classList.remove('shown');
	document.getElementById(name).classList.add('shown');

document.getElementById('menu-watch').classList.remove('shown');
document.getElementById('menu-trailer').classList.remove('shown');
document.getElementById('menu-info').classList.remove('shown');
document.getElementById('menu-extras').classList.remove('shown');
document.getElementById('menu-links').classList.remove('shown');
document.getElementById('menu-collections').classList.remove('shown');

document.getElementById('menu-' + name).classList.add('shown');
}

function goBack(){
	$('body')[0].innerHTML = list_body;
	var html = "";
	for(var i = 0; i < Object.keys(catalog).length; i++){
		if(Object.keys(catalog)[i] != "info") { //TODO: validate that this is movie item?
			var poster = getLangedValue(movie_list[Object.keys(catalog)[i]].posters, "value");
			if(poster == null){
				poster = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Kodak_Portra_800_135_film_cartridge_01.jpg/160px-Kodak_Portra_800_135_film_cartridge_01.jpg";
			}
			html += "<div class='menu-poster'>" + "<a href='#' onClick=\"alert('TO BE DEVELOPED: This hides the movie from all views. It will only be accessible via search, suggested in the HIDDEN ITEMS section')\" class='hide-icon' ><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/OOjs_UI_icon_eyeClosed.svg/20px-OOjs_UI_icon_eyeClosed.svg.png?20180610085129'></a>" + 
			"<a href='#' onClick=\"getMovie('" + Object.keys(catalog)[i] + "')\"><img src='" + poster + "' /></a></div>";
		}
	}
	$('#list')[0].innerHTML = html;
}
