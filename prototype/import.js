
var movie_list = {};
var collection_list = {};
var catalog = {};

function importCatalog(catalog){
	for (var i = 0; i < Object.keys(catalog).length; i++){
		var movie_qid = Object.keys(catalog)[i];
		if (movie_qid != "info" && movie_list[movie_qid] == undefined) {
			queryMovie(movie_qid);
		}
	}
}

function queryMovie(movie_qid) {
	movie_list[movie_qid] = { id: movie_qid };

	$.ajax({
  		type: 'GET',
  		url: 'https://www.wikidata.org/wiki/Special:EntityData/' + movie_qid + '.json',
	}).then(function(payload) {
		var movie_qid = Object.keys(payload.entities)[0];
		var movie = movie_list[movie_qid];
		movie.film = containsValue(payload.entities[movie_qid].claims["P31"], filmSubclasses);
		movie.label = getLangedValue(payload.entities[movie_qid].labels, "value");

		if(movie.film == false){
			return;
		}

		movie.gender_ids = getIds(payload.entities[movie_qid].claims["P136"]);
		movie.country_ids = getIds(payload.entities[movie_qid].claims["P495"]);
		movie.director_ids = getIds(payload.entities[movie_qid].claims["P57"]);
		movie.screenwriter_ids = getIds(payload.entities[movie_qid].claims["P58"]);
		movie.cast_ids = getIds(payload.entities[movie_qid].claims["P161"]);
		movie.companies_ids = getIds(payload.entities[movie_qid].claims["P272"]);
		movie.date = getFirstValue(payload.entities[movie_qid].claims["P577"], "time");
		movie.duration = getFirstValue(payload.entities[movie_qid].claims["P2047"], "amount");
		
		var original_language = getFirstValue(payload.entities[movie_qid].claims["P364"], "id");
		if(original_language != null){
			movie.original_language = app_wikidatalangs[original_language];
		}

		//RELATED
		var collections = payload.entities[movie_qid].claims["P179"];
		movie.collections_ids = getIds(collections, true);
	
		movie.followedBy = getFirstValue(payload.entities[movie_qid].claims["P156"], "id");
		if (movie.followedBy == null) {
			movie.followedBy = getFirstValueFromQualifiers(collections, "P156");
		}

		movie.follows = getFirstValue(payload.entities[movie_qid].claims["P155"], "id");
		if (movie.follows == null) {
			movie.follows = getFirstValueFromQualifiers(collections, "P155");
		}

		if (movie.followedBy != null && movie_list[movie.followedBy] == undefined) {
			queryMovie(movie.followedBy);
		}

		if (movie.follows != null && movie_list[movie.follows] == undefined) {
			queryMovie(movie.follows);
		}

		movie.based = listMovies(payload.entities[movie_qid].claims["P144"]);
		movie.derivative = listMovies(payload.entities[movie_qid].claims["P4969"]);
		//END - RELATED

		//wikis, commons, quotes
		movie.wikis = {};
		movie.quotes = {};
		movie.commons = [];
		var sitelinks = payload.entities[movie_qid].sitelinks;
		if (sitelinks != undefined) {
			var keys = Object.keys(sitelinks); 
			for (var i = 0; i < keys.length; i++){
				if (keys[i].indexOf("wikiquote") != -1){
					movie.quotes[keys[i].substr(0, keys[i].indexOf("wikiquote"))] = sitelinks[keys[i]].url;
				} else if (keys[i].indexOf("commons") != -1){
					movie.commons.push(sitelinks[keys[i]].url);
				} else {
					movie.wikis[keys[i].substr(0, keys[i].indexOf("wiki"))] = sitelinks[keys[i]].url;
				}
			}
		}

		//PLOT
		var langedWiki = getLangedValue(movie.wikis);
		if (langedWiki != null && langedWiki != "") {
			$.ajax({
				type: 'GET',
				url: langedWiki.replace('/wiki/','/api/rest_v1/page/summary/'),
			}).then(function(m2) {
				if (m2.extract != undefined && m2.extract != null) {
					movie_list[m2.wikibase_item].plot = m2.extract;
				}
			});
		}

		//collections
		if (movie.collections_ids != undefined && movie.collections_ids != null) {
			for (var i = 0; i < movie.collections_ids.length; i++){
				if(collection_list[movie.collections_ids[i]] == undefined){
					$.ajax({
						type: 'GET',
						url: 'https://www.wikidata.org/wiki/Special:EntityData/' + movie.collections_ids[i] + '.json',
					}).then(function(msg) {
						var eid = Object.keys(msg.entities)[0];
						var entity = msg.entities[eid];
						if (entity.claims.P527 != undefined && entity.claims.P527.length > 0) {
							collection_list[eid] = {
								sagaTitle: getLangedValue(entity.labels, "value"),
								movieList: listMovies(entity.claims.P527)
							}
						}
					});
				}
			}
		}


		//posters
		movie.posters = {};
		var images = payload.entities[movie_qid].claims["P3383"];
		if (images != undefined && Object.keys(images).length > 0) {
			for (var i = 0; i < Object.keys(images).length; i++) {
				var poster = null;
				var reference = null;
				if (images[i].mainsnak.datavalue != undefined) {
					poster = images[i].mainsnak.datavalue.value;
					if(images[i].references != undefined && images[i].references.length > 0) {
						if (images[i].references[0].snaks != undefined && Object.keys(images[i].references[0].snaks).length > 0) {
							var p = images[i].references[0].snaks[images[i].references[0]["snaks-order"][0]];
							if (p != undefined && p.length > 0 && p[0].datavalue != undefined) {
								reference = p[0].datavalue.value;
							}	
						}
					}
				}
			var entity = {};
			entity.value = poster;
			entity.reference = reference;
			//movie.posters[i] = entity;
			}
		}

		var wikis = Object.keys(movie.wikis);
		for (var i = 0; i < wikis.length; i++){
			//TO AVOID CROSS ORIGIN GRB
			if (wikis[i] != "en"){
				continue;
			}
			$.ajax({
				type: 'GET',
				url: movie.wikis[wikis[i]],
			}).then(function(m1) {
				var html = $(m1);
				var poster = html.find(".infobox img");
				if (poster != undefined && poster != null && poster.length > 0) {
					poster = poster[0].src;
					var reference = html.find(".infobox-caption");
					if (reference != undefined && reference != null && reference.length > 0) {
						reference = [0].innerText;
					} else {
						reference = null;
					}
					var qid = html.find("#t-wikibase a")[0].href;
					qid = qid.substr(qid.lastIndexOf("/") + 1);
					var lang = poster.substring("https://upload.wikimedia.org/wikipedia/".length);
					lang = lang.substr(0, lang.indexOf("/"));
					var entity = {};
					entity.value = poster;
					entity.reference = reference;
					movie_list[qid].posters[lang] = entity;
				}
				var commons = html.find("li.wb-otherproject-commons a");
				if(commons != undefined && commons != null && commons.length > 0){
					movie.commons.push(commons[0].href);
				}
			});
		}
		//END - posters

		if(catalog[movie_qid] != undefined){ //avoid useless requests of sources from movies not available in catalog
			for(var i = 0; i < catalog[movie_qid].feature.length; i++){
				checkLocation(catalog[movie_qid].feature[i].url);
			}
		}
		

	}).catch(function(xmlHttpRequest, statusText, errorThrown) {
  		alert('Your form submission failed.\n\n'
     		+ 'XML Http Request: ' + JSON.stringify(xmlHttpRequest)
      		+ ',\nStatus Text: ' + statusText
      		+ ',\nError Thrown: ' + errorThrown);
	});
}

function queryEntity(entity_qid){
	$.ajax({
		type: 'GET',
		url: 'https://www.wikidata.org/wiki/Special:EntityData/' + entity_qid + '.json',
	}).then(function(payload) {
		dictionary[entity_qid] = getLangedValue(payload.entities[entity_qid].labels, "value");
	});
}

function getLangedValue(array, value){
	var result = null;
	if (array != undefined && Object.keys(array).length > 0) {
		for(var i = 0; i < conf_databaselangs.length; i++){
			if (array[conf_databaselangs[i]] != undefined) {
				result = value != undefined ? array[conf_databaselangs[i]][value]: array[conf_databaselangs[i]];
				break;
			}
		}
		if (result == null) {
			if (array[app_databasefallbacklang] != undefined) {
				result = value != undefined ? array[app_databasefallbacklang][value] : array[app_databasefallbacklang];
			} else {
				result = value != undefined ? array[Object.keys(array)[0]][value] : array[Object.keys(array)[0]];
			}
		}
	}
	return result;
}

function getIds(array, noEntityQuery){
	var list = null;
	if (array != undefined){
		var count = array.length;
		list = [];
		for(var i = 0; i < count; i++){
			if(array[i].mainsnak.datavalue != undefined){
				list[i] = array[i].mainsnak.datavalue.value.id;
				if (dictionary[list[i]] == undefined) {
					dictionary[list[i]] = null;
					if (noEntityQuery != true){
						queryEntity(list[i]);
					}
				}
			}
		}
	}
	return list;
}

function getFirstValue(property, value){
	var result = null;
	if (property != undefined) {
		for (var i = 0; i < property.length; i++){
			if (property[i].mainsnak.datavalue != undefined){
				result = property[i].mainsnak.datavalue.value[value];
				break;
			}
		}
	}
	return result;
}

function getFirstValueFromQualifiers(collections, property){
	var result = null;
	if (collections != undefined){
		for (var i = 0; i < collections.length; i++){
			if (collections[i].qualifiers != undefined && collections[i].qualifiers[property] != undefined){
				for (var j = 0; j < collections[i].qualifiers[property].length; j++){
					if (collections[i].qualifiers[property][j].datavalue != undefined){
						result = collections[i].qualifiers[property][j].datavalue.value.id;
						break;
					}
				}
			}
		}
	}
	return result;
}

function listMovies(properties){
	var list = [];
	if (properties != undefined){
		for (var i = 0; i < properties.length; i++){
			if(properties[i].mainsnak.datavalue != undefined){
				list[i] = properties[i].mainsnak.datavalue.value.id;
				if (movie_list[list[i]] == undefined){
					queryMovie(list[i]);
				}
			}
		}
	}
	return list;
}

function containsValue(properties, values){
	if (properties != undefined){
		for (var i = 0; i < properties.length; i++){
			if(properties[i].mainsnak.datavalue != undefined && values.indexOf(properties[i].mainsnak.datavalue.value.id) != -1){
				return true;
			}
		}
	}
	return false;
}

function openImport(){
	document.getElementById('import').classList.add('shown');
}

function ximport(){
	catalog = JSON.parse(document.getElementById('import_text').value);
	importCatalog(catalog);
}

function xport(){
	document.getElementById('import').classList.add('shown');
	document.getElementById('import-button').classList.add('hide');
	document.getElementById('import_text').value = JSON.stringify(catalog);
}
