var app_wikipedias = null;
var app_wikilangues = null;
var app_wikimediaLangs = [];

var dictionary = {};

var app_wikidatalangs = {};
var app_interfacelangs = ["en", "es", "fr"];
var conf_interfacelang = "es";
var conf_databaselangs = ["es", "fr"];
var app_databasefallbacklang = "en"; //in the languages query it is forced, since there is no further fallback

var dbLabels = {};

function makeSPARQLQuery( endpointUrl, sparqlQuery, doneCallback ) {
	var settings = {
		headers: { Accept: 'application/sparql-results+json' },
		data: { query: sparqlQuery }
	};
	return $.ajax( endpointUrl, settings ).then( doneCallback );
}

var endpointUrl = 'https://query.wikidata.org/sparql';
var sparqlWikipediaQuery = "#URL de Wikipedia en todos los idiomas\n" +
        "#title: URLs of Wikipedia in all languages\n" +
        "SELECT ?wikipedia WHERE {\n" +
        "  ?wikipedia wikibase:wikiGroup \"wikipedia\".\n" +
        "}";

function queryAndBuildLangs(){
	var sparqlQuery = "SELECT DISTINCT ?lang_code ?itemLabel ?item\n" +
        "WHERE\n" +
        "{\n" +
        "  # ?lang is one of these options\n" +
        "  VALUES ?lang {\n" +
        "    wd:Q34770   # language\n" +
        "    wd:Q436240  # ancient language\n" +
        "    wd:Q1288568 # modern language\n" +
        "    wd:Q33215   # constructed language\n" +
        "  }\n" +
        "  ?item wdt:P31 ?lang ;\n" +
        "    # get the language code\n" +
        "    wdt:P424 ?lang_code .\n" +
        "  SERVICE wikibase:label { bd:serviceParam wikibase:language \"" + conf_interfacelang + "\",\"en\". }\n" +
        "} ORDER BY ?lang_code";

	makeSPARQLQuery( endpointUrl, sparqlQuery, function( data ) {
		app_wikilangues = data.results.bindings;

		makeSPARQLQuery( endpointUrl, sparqlWikipediaQuery, function( data ) {
				//$( 'body' ).append( $( '<pre>' ).text( JSON.stringify( data ) ) );
				app_wikipedias = data.results.bindings;
				buildWikimediaLangs();
			}
		);
	}
	);
}

function buildWikimediaLangs(){
	for(var i = 0; i < app_wikipedias.length; i++){
		if(app_wikipedias[i].wikipedia.value.indexOf("https://") == 0){
			var  uri = app_wikipedias[i].wikipedia.value.substr(8);
			app_wikimediaLangs.push(uri.substr(0, uri.indexOf('.')));
		}
	}
	for(var i = 0; i < app_wikilangues.length; i++){
		if(app_wikimediaLangs.indexOf(app_wikilangues[i].lang_code.value)>-1){
			var id = app_wikilangues[i].item.value.substr("http://www.wikidata.org/entity/".length);
			var label = app_wikilangues[i].itemLabel.value;
			app_wikidatalangs[id] = app_wikilangues[i].lang_code.value;
			dictionary[id] = label;
		}
	}
	app_wikipedias = undefined;
	app_wikilangues = undefined;
	app_wikimediaLangs = undefined;
}

queryAndBuildLangs();
getLabels();

//film subclasses
var filmSubclasses = [];

class SPARQLQueryDispatcher {
	constructor( endpoint ) {
		this.endpoint = endpoint;
	}

	query( sparqlQuery ) {
		const fullUrl = this.endpoint + '?query=' + encodeURIComponent( sparqlQuery );
		const headers = { 'Accept': 'application/sparql-results+json' };

		return fetch( fullUrl, { headers } ).then( body => body.json() );
	}
}

const sparqlFilmQuery = `SELECT ?item
WHERE
{
  ?item wdt:P279+ wd:Q11424 .
 }`;

const queryDispatcher = new SPARQLQueryDispatcher( endpointUrl );
queryDispatcher.query( sparqlFilmQuery ).then( buildFilmInstances );

function buildFilmInstances(msg){
	var items = msg.results.bindings;
	for(var i = 0; i < items.length; i++){
		filmSubclasses.push(items[i].item.value.substr("http://www.wikidata.org/entity/".length));
	}
	filmSubclasses.push("Q11424");
}

function getLabels(){
	dbLabels = {
		P136: { reference: "genders"},
		P1476: { reference: "title"},
		P495 : { reference: "countries of origin" },
		P57: { reference: "director" },
		P58: { reference: "screenwritters" },
		P161: { reference: "cast members" },
		P577: { reference: "publication date" },
		P2047: { reference: "duration" },
		P179: { reference: "part of the series" },
		P272: { reference: "production companies" }
	};
	for (var i = 0; i < Object.keys(dbLabels).length; i++){
		$.ajax({
			type: 'GET',
			url: 'https://www.wikidata.org/wiki/Special:EntityData/' + Object.keys(dbLabels)[i] + '.json',
		}).then(function(m2) {
			dbLabels[Object.keys(m2.entities)[0]].value = getLangedValue(m2.entities[Object.keys(m2.entities)[0]].labels);
		});
	}
}
