var movie_to_add;
function add(){
	document.getElementById('modal').classList.add('shown');
	movie_to_add = { feature: [] };
	document.getElementById('id-sourcesnum').value = movie_to_add.feature.length;
}

function addSource(){
	var movie_registration = document.getElementById('id-registration').checked === true ? 1 : 0;
	var movie_hq = document.getElementById('id-hq').checked === true ? 1 : 0;

	var movie_source = {
		url: document.getElementById('id-link').value,
		status: 1,
		update: (new Date()).toISOString().substr(0,10),
		//provider: document.getElementById('id-provider').value,
		registration: movie_registration,
		cost: document.getElementById('id-cost').value,
		//offline: Number(document.getElementById('id-offline').value),
		audios: document.getElementById('id-audio').value.split("|"),
		hq: movie_hq
	};

	if(document.getElementById('id-subtitle').value != ""){
		movie_source.subtitles = document.getElementById('id-subtitle').value.split("|");
	}

	movie_to_add.feature.push(movie_source);

	document.getElementById('id-sourcesnum').value = movie_to_add.feature.length;
	document.getElementById('id-link').value = "";
}

function addMovie(){
	document.getElementById('modal').classList.remove('shown');
	var qid = document.getElementById('id-wikidata').value;
	catalog[qid] = movie_to_add;
	queryMovie(qid);
}

//ID
function checkMovie(){
	var qid = document.getElementById('id-wikidata').value;
	queryMovie(qid);
	setTimeout(checkMovieAction, 3000);//TODO: VERIFY THAT IT IS A MOVIE
}

function checkMovieAction(){
	document.getElementById('id-wikidata-c').innerText = movie_list[document.getElementById('id-wikidata').value].label;
}

//Location
var host_provider = {};
function getHost(url){
	var result = null;
	var url_obj = null;
	if(url != null && url != ""){
		try{
			url_obj = new URL(url);
		} catch (ex) {
		}
	}
	if(url_obj != null){
		result = url_obj.host;
	}
	return result;
}

//VERIFY
function checkLocation(url){
	var host = getHost(url);
	if(host != null){
		if(host_provider[host] != undefined){
			document.getElementById('id-link-c').innerText = dictionary[host_provider[host]];
		} else {
			queryProvider(host);
		}
	}
}

function queryProvider(host){
	var webs = [];
	
//https://
	webs.push("https://" + host);

//http://
	webs.push("http://" + host);

//no http
	webs.push(host);

//TODO - unsuported in JS
//www
//no www
//particular subdomain

// end / and no end /
	var filters = [];
	for(var i = 0; i < webs.length; i++){
		filters.push(webs[i]);
		filters.push(webs[i] + "/");
	}

	var sparqlProviderQuery = "SELECT ?item ?itemLabel WHERE { ?item wdt:P856 ?web . FILTER(";
	var filtersStr = "";
        for(var i = 0; i < filters.length; i++){
		filtersStr += "?web = URI(\"" + filters[i] + "\") ||";
	}
	if(filtersStr.length > 2){
		filtersStr = filtersStr.substr(0, filtersStr.length - 3);
	}
	sparqlProviderQuery += filtersStr + ")}";

	host_provider[host] = "";

	makeSPARQLQuery( endpointUrl, sparqlProviderQuery, function( data ) {
			//document.getElementById('id-link-c').innerText = getLangedValue(payload.entities[movie_qid].labels, "value");
			var items = data.results.bindings;
			for(var i = 0; i < items.length; i++){
				var id = items[i].item.value.substr("http://www.wikidata.org/entity/".length);
				host_provider[host] = id;
				queryEntity(id);
				setTimeout(document.getElementById('id-link-c').innerText = dictionary[id], 3000);
			}
		}
	);
}
