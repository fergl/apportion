SOURCE CODE
-----------
Apportion™ is a software project originally created by Fernando Lamata in 2022, released under an MIT Licence and published on gitlab.com. Corresponding licence can be found [here](./LICENSE).
This MIT Licence is limited only to the text source files of the project and does not licence any image included in this repository or gives away any right corresponding to the Apportion™ trademark.

TRADEMARK
---------
The name Apportion™ constitutes a trademark owned by Fernando Lamata. None of the licenses quoted in this document grant permission to use the Apportion™ trademark. Apportion™ trademark is not intended and must not be used in any derivated work and shall not be used in any other software project. Nominative faire use of the Apportion™ trademark is allowed when mentioning this work.
"Apportion" can also be referred as software title, software name, project title and/or project name.

